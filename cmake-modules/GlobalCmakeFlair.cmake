if(DEFINED GLOBALCMAKEFLAIR_INCLUDED)
    return()
endif()

SET(GLOBALCMAKEFLAIR_INCLUDED true)

include($ENV{FLAIR_ROOT}/flair-dev/cmake-modules/ColoredMessage.cmake)

#check if we have a flair-hds directory
if (IS_DIRECTORY $ENV{FLAIR_ROOT}/flair-hds )
	info("found flair-hds directory")
	SET(FOUND_HDS_DIR TRUE)	
endif()

if(NOT DEFINED ENV{FLAIR_ROOT})
	message(FATAL_ERROR  "variable FLAIR_ROOT not defined")
endif()

if(NOT DEFINED ENV{FLAIR_ROOT})
	err("variable FLAIR_ROOT not defined")
endif()

if(NOT FLAIR_DEV)
	IF(FLAIR_DEV_TAG)
		warn("Configuring flair-dev and flair-bin for tag ${FLAIR_DEV_TAG}")
		SET(FLAIR_DEV $ENV{FLAIR_ROOT}/flair-dev_svn/tags/${FLAIR_DEV_TAG})
		SET(FLAIR_BIN $ENV{FLAIR_ROOT}/flair-bin_svn/tags/${FLAIR_DEV_TAG})
		if(EXISTS "${FLAIR_DEV}/cmake-modules/GlobalCmakeUAV.cmake")
			UNSET(FLAIR_DEV_TAG)
			include(${FLAIR_DEV}/cmake-modules/GlobalCmakeUAV.cmake)
			return()
		else()
		   	err("File not found ${FLAIR_DEV}/cmake-modules/GlobalCmakeUAV.cmake Please check that ${FLAIR_DEV} is up to date")
		endif()
	ELSE()
		SET(FLAIR_DEV $ENV{FLAIR_ROOT}/flair-dev)
		SET(FLAIR_BIN $ENV{FLAIR_ROOT}/flair-bin)
	ENDIF()
ENDIF()

include(${FLAIR_DEV}/cmake-modules/ArchDir.cmake)

list(APPEND CMAKE_MODULE_PATH ${FLAIR_DEV}/cmake-modules/)

#framework
SET(FLAIR_USE_FILE ${FLAIR_DEV}/cmake-modules/FlairUseFile.cmake)

#default executable ouput paths
if(NOT DEFINED EXECUTABLE_OUTPUT_PATH)
	SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
endif()
if(NOT DEFINED TARGET_EXECUTABLE_OUTPUT_PATH)
	SET(TARGET_EXECUTABLE_OUTPUT_PATH bin)
endif()

#add definitions for archs
if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "armv7a-neon")	
    ADD_DEFINITIONS(-DARMV7A)
endif()

if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "armv5te")	
    ADD_DEFINITIONS(-DARMV5TE)
endif()

if("${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE}" MATCHES "core2-64")	
    ADD_DEFINITIONS(-DCORE2_64)
endif()

#reimplement add executable to add a custom target for delivery (only on ARM)
#delivery are read from ssh config file
#also add a message to display architecture
function(ADD_EXECUTABLE)
	if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm" AND EXISTS "$ENV{HOME}/.ssh/config")
		file(STRINGS $ENV{HOME}/.ssh/config TEST)
		foreach(f ${TEST})
			string(FIND ${f} "Host " POS)#cherche ligne host
			if(${POS} GREATER -1)
				string(REPLACE Host "" TARGET_NAME ${f})#enleve Host
				string(STRIP ${TARGET_NAME} TARGET_NAME)#enleve les espaces
			endif()
			string(FIND ${f} HostName POS)#cherche hostname
			if(${POS} GREATER -1)
				string(FIND ${f} "192.168." POS)#cherche addresse
				if(${POS} GREATER 0)#garde que les adresses en 192.168.x.x
					string(REPLACE HostName "" ADDRESS ${f})#enleve Hostname
					string(STRIP ${ADDRESS} ADDRESS)#enleve les espaces
					#message("adding delivery target for " ${ARGV0} " (" ${ADDRESS} ")")
					string(REPLACE "/" "_" TARGET_PATH ${TARGET_EXECUTABLE_OUTPUT_PATH})#les / ne sont pas acceptés
					add_custom_target(
					    delivery_root_${ADDRESS}_${TARGET_PATH}_${ARGV0}
					    COMMAND make
					    COMMAND scp ${EXECUTABLE_OUTPUT_PATH}/${ARGV0} root@${ADDRESS}:${TARGET_EXECUTABLE_OUTPUT_PATH}
					)
				endif()
			endif()
		endforeach(f) 
	endif()

    #call original function
	_ADD_EXECUTABLE(${ARGV})

    #add a message to display architecture
    string(SUBSTRING ${ARGV0} 0 5 TESTNAME) #check if it is qt4 internal target
    if(NOT ${TESTNAME} MATCHES "Qt4::")
        add_custom_command(TARGET ${ARGV0} POST_BUILD
            COMMENT "${ARGV0} built for ${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE} architecture")
    endif()

    #add a dependance to FlairLibs and FlairHdsLibs if we use the top level CMakeLists.txt and we need flairlibs
    if(TARGET FlairLibs AND DEFINED FLAIRUSEFILE_INCLUDED)
        add_dependencies(${ARGV0} FlairLibs)
    endif()
    if(TARGET FlairHdsLibs AND DEFINED FLAIRUSEFILE_INCLUDED)
        add_dependencies(${ARGV0} FlairHdsLibs)
    endif()
endfunction()


#reimplement add library to add a message to display architecture
function(ADD_LIBRARY)
    #call original function
	_ADD_LIBRARY(${ARGV})
    string(SUBSTRING ${ARGV0} 0 5 TESTNAME) #check if it is qt4 internal target
    if(NOT ${TESTNAME} MATCHES "Qt4::")
        add_custom_command(TARGET ${ARGV0} POST_BUILD
            COMMENT "${ARGV0} built for ${CMAKE_SYSTEM_PROCESSOR_DEFAULTTUNE} architecture")
    endif()
endfunction()

#copy resources (ex: demos scripts and xml)
function(INSTALL)
    if(COPY_RESOURCE_FILES)
        FILE(GLOB RESOURCE_FILES "${CMAKE_CURRENT_SOURCE_DIR}/resources/${ARCH_DIR}/*")
        foreach(item IN LISTS RESOURCE_FILES)
            get_filename_component(filename ${item} NAME)
            #do not overwrite: user can change xml or scripts 
            _INSTALL(CODE "
                if (NOT EXISTS \"$ENV{FLAIR_ROOT}/flair-bin/demos/${ARCH_DIR}/${PROJECT_NAME}/${filename}\")
                    file(INSTALL \"${item}\" DESTINATION \"$ENV{FLAIR_ROOT}/flair-bin/demos/${ARCH_DIR}/${PROJECT_NAME}\" USE_SOURCE_PERMISSIONS)
                endif()
            ")
        endforeach()

    endif()
    #call original function
	_INSTALL(${ARGV})
endfunction()
