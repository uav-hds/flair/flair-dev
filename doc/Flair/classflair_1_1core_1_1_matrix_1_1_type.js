var classflair_1_1core_1_1_matrix_1_1_type =
[
    [ "Type", "classflair_1_1core_1_1_matrix_1_1_type.html#a993a8210fdc5f834954fb36f9583612a", null ],
    [ "GetSize", "classflair_1_1core_1_1_matrix_1_1_type.html#a2bd0b382b8bb50983cdb2caa8bff8b02", null ],
    [ "GetDescription", "classflair_1_1core_1_1_matrix_1_1_type.html#a0f27d3c6f5b59642decb368a3e0ea0d1", null ],
    [ "GetNbRows", "classflair_1_1core_1_1_matrix_1_1_type.html#a3cb53338ec0a58b946073e4d2b56c1b9", null ],
    [ "GetNbCols", "classflair_1_1core_1_1_matrix_1_1_type.html#ab2b715ddb4afa63c5373c384b629aca2", null ],
    [ "GetElementDataType", "classflair_1_1core_1_1_matrix_1_1_type.html#a524df39b8954c7aa1772ed3df7bc7fa1", null ]
];