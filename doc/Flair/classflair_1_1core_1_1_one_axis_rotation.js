var classflair_1_1core_1_1_one_axis_rotation =
[
    [ "RotationType_t", "classflair_1_1core_1_1_one_axis_rotation.html#a327edc6a6cdc6af2abc763aea6618495", [
      [ "PreRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a327edc6a6cdc6af2abc763aea6618495a557e83099f72841e6606346c9e5f586b", null ],
      [ "PostRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a327edc6a6cdc6af2abc763aea6618495abaf82373282063be289c070dda3d3a07", null ]
    ] ],
    [ "OneAxisRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a85a24d18b48761166caa4f037b13ebb4", null ],
    [ "~OneAxisRotation", "classflair_1_1core_1_1_one_axis_rotation.html#adb99d2f257c0600f9d56813d1b72fe6b", null ],
    [ "ComputeRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a59b74b5059d847710803259bd1b8f1c1", null ],
    [ "ComputeRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a9326b1fdeae7a485235c8bec5191fcde", null ],
    [ "ComputeRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a95eafcd87e97beacc6e11370a8dde4ff", null ],
    [ "ComputeRotation", "classflair_1_1core_1_1_one_axis_rotation.html#a3aeb426d4ddd9d33447b86887539eeb5", null ],
    [ "GetAngle", "classflair_1_1core_1_1_one_axis_rotation.html#a311d0fbae6722ce463f8f20a1c17545b", null ],
    [ "GetAxis", "classflair_1_1core_1_1_one_axis_rotation.html#af3f1d2cbb28501721998f0ccbbdf6710", null ]
];