var NAVTREE =
[
  [ "Flair", "index.html", [
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_afro_bldc_8h.html",
"_unix___serial_port_8h.html",
"classflair_1_1core_1_1_gps_data.html#ae2cac37d2ddaea43014053c88790a33da9bec9e2ee8e19932559e21b79482f5f2",
"classflair_1_1core_1_1_thread.html#ab58a00466581b6cf88ba28b8e2118c65",
"classflair_1_1filter_1_1_pid.html#a54ef7c5dc588f52954121e22faf50b92",
"classflair_1_1meta_1_1_meta_us_range_finder.html#a4b89240f6433abecd09ecc2f19eb1746",
"classflair_1_1sensor_1_1_laser_range_finder.html#a00cc6a10c9f6bc146dd269acd347b72f",
"classflair_1_1simulator_1_1_discrete_time_variable.html#ae9c2b7ffd8e1554686389aff8e854d43"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';