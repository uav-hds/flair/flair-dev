var classflair_1_1core_1_1_framework_manager =
[
    [ "FrameworkManager", "classflair_1_1core_1_1_framework_manager.html#ac4488013eb79fb1cd0c57fe4774120ae", null ],
    [ "~FrameworkManager", "classflair_1_1core_1_1_framework_manager.html#aa97c7fbaba572aa856d23c416f3b74e4", null ],
    [ "SetupConnection", "classflair_1_1core_1_1_framework_manager.html#a6f3fb7e3a6eb077f6330475791f3d00b", null ],
    [ "SetupUserInterface", "classflair_1_1core_1_1_framework_manager.html#a528ab69c3a850237007a074732d9fa85", null ],
    [ "GetTabWidget", "classflair_1_1core_1_1_framework_manager.html#a8f3989b0199826a913f352cc1e26f86b", null ],
    [ "SetupLogger", "classflair_1_1core_1_1_framework_manager.html#a87827cc05ace9d15c13be60d231d9ab3", null ],
    [ "GetLogPath", "classflair_1_1core_1_1_framework_manager.html#a4e7363801f1df89e51fd9c173e196673", null ],
    [ "AddDeviceToLog", "classflair_1_1core_1_1_framework_manager.html#a9f2750ce913b492206761340c666bbb6", null ],
    [ "IsDeviceLogged", "classflair_1_1core_1_1_framework_manager.html#aa426692b6ffdcacc35c34f0db87d53b7", null ],
    [ "StartLog", "classflair_1_1core_1_1_framework_manager.html#a1cbc5e8296c0f36551210e13251516ae", null ],
    [ "StopLog", "classflair_1_1core_1_1_framework_manager.html#a978aae4beac0c20dbbff73369ab6e31a", null ],
    [ "IsLogging", "classflair_1_1core_1_1_framework_manager.html#a65eff6c3651dafddc2dd55c3cab281b4", null ],
    [ "UpdateSendData", "classflair_1_1core_1_1_framework_manager.html#a6ed9bdb73288176aa58269f8a520d58c", null ],
    [ "BlockCom", "classflair_1_1core_1_1_framework_manager.html#aa1b6474b3972665d20c706ff89965271", null ],
    [ "UnBlockCom", "classflair_1_1core_1_1_framework_manager.html#a311fa9be9c08b037cae24061e85307d1", null ],
    [ "ConnectionLost", "classflair_1_1core_1_1_framework_manager.html#a845ac3ad0cbc95d71e50277d9e8093a1", null ],
    [ "DisableErrorsDisplay", "classflair_1_1core_1_1_framework_manager.html#a69e12f51081a3d58876553d9858ea1bd", null ],
    [ "IsDisplayingErrors", "classflair_1_1core_1_1_framework_manager.html#a477b3cee8fa2c2338896006faaeefe42", null ]
];