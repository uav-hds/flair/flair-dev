var classflair_1_1sensor_1_1_target_controller =
[
    [ "TargetController", "classflair_1_1sensor_1_1_target_controller.html#aea11e29bf952913cc648dfc59dac215f", null ],
    [ "~TargetController", "classflair_1_1sensor_1_1_target_controller.html#af7f9a61889cd8ebe05074e977b15d68f", null ],
    [ "IsConnected", "classflair_1_1sensor_1_1_target_controller.html#a2497d1f4174ce152f3b399f8a2c389fc", null ],
    [ "IsDataFrameReady", "classflair_1_1sensor_1_1_target_controller.html#a303f53ce0bae95c47688678e4eb52d4c", null ],
    [ "GetAxisNumber", "classflair_1_1sensor_1_1_target_controller.html#a755c01ac4c8596e94310b5d61356be0c", null ],
    [ "GetAxisName", "classflair_1_1sensor_1_1_target_controller.html#a8769ff6c7f3887a9bc0f8215f0a76a14", null ],
    [ "GetAxisValue", "classflair_1_1sensor_1_1_target_controller.html#ac3ce201706dacaa107f59697a48dded1", null ],
    [ "GetButtonNumber", "classflair_1_1sensor_1_1_target_controller.html#afa9256a134754f3cd321009f0c721e4f", null ],
    [ "IsButtonPressed", "classflair_1_1sensor_1_1_target_controller.html#ab882d1b43a3d2ea0c60fbe95f53d5b1b", null ],
    [ "GetButtonName", "classflair_1_1sensor_1_1_target_controller.html#a8f79eb9486927826a85a4ca3eb61ac37", null ],
    [ "IsControllerActionSupported", "classflair_1_1sensor_1_1_target_controller.html#aacd8adc6f113fa88cad434cd26bacd2e", null ],
    [ "SetLedOn", "classflair_1_1sensor_1_1_target_controller.html#a2763e37d807239f187e23b1b2675940c", null ],
    [ "SetLedOff", "classflair_1_1sensor_1_1_target_controller.html#a08ffd72ab93badce223eae5c92d4ee12", null ],
    [ "Rumble", "classflair_1_1sensor_1_1_target_controller.html#ad224c144a6207dbcc8b8160a34b8aa4b", null ],
    [ "FlashLed", "classflair_1_1sensor_1_1_target_controller.html#a3c1926b41302b0487acfec388b79a26f", null ],
    [ "UpdateFrom", "classflair_1_1sensor_1_1_target_controller.html#a1e883144c9ebf38961b7bce349fd8da3", null ],
    [ "GetTab", "classflair_1_1sensor_1_1_target_controller.html#a0193e57e4544a07e98e51443ce7f3390", null ],
    [ "ProcessMessage", "classflair_1_1sensor_1_1_target_controller.html#a1412843c3868da9d8b6ff320badd7766", null ],
    [ "QueueMessage", "classflair_1_1sensor_1_1_target_controller.html#a677a1fd35d04b1ffd033206634c6c05e", null ],
    [ "ControllerInitialization", "classflair_1_1sensor_1_1_target_controller.html#a8aa84807cf224d3af89ee8af0355992c", null ],
    [ "AcquireAxisData", "classflair_1_1sensor_1_1_target_controller.html#a073753df5ba71c094650540556352017", null ],
    [ "AcquireButtonData", "classflair_1_1sensor_1_1_target_controller.html#a74c24d2f0e7e1407b04861be9ef309e9", null ],
    [ "axisNumber", "classflair_1_1sensor_1_1_target_controller.html#a1fb5ecb35823de3f5e201f3c2cb1d468", null ],
    [ "axis", "classflair_1_1sensor_1_1_target_controller.html#a24e9fd05a49cc89320214bff51a7e18a", null ],
    [ "bitsPerAxis", "classflair_1_1sensor_1_1_target_controller.html#a122a460a208f5e4d08a222af44c2e46f", null ],
    [ "buttonNumber", "classflair_1_1sensor_1_1_target_controller.html#a3c056f88a6fe47f2be02ef184986282c", null ],
    [ "button", "classflair_1_1sensor_1_1_target_controller.html#a06d2f95fb458a4178e3e733ce809fe7a", null ],
    [ "ledNumber", "classflair_1_1sensor_1_1_target_controller.html#a2a86fea6048d3b3f71a898733915306b", null ]
];