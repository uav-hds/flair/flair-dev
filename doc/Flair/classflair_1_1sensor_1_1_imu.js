var classflair_1_1sensor_1_1_imu =
[
    [ "Imu", "classflair_1_1sensor_1_1_imu.html#a6d89cc906fe0eeb233aa50bf59f99bb3", null ],
    [ "Imu", "classflair_1_1sensor_1_1_imu.html#a5dc17ecf8caf9aaa41ec0a6ec43adf6a", null ],
    [ "~Imu", "classflair_1_1sensor_1_1_imu.html#ab01ce2e6b89698b001da63ae8df1722a", null ],
    [ "GetDatas", "classflair_1_1sensor_1_1_imu.html#adb1105fe7b1c21234b6edde4d64da428", null ],
    [ "GetLayout", "classflair_1_1sensor_1_1_imu.html#a1d74dd0b750c287b112489786ff5b7eb", null ],
    [ "LockUserInterface", "classflair_1_1sensor_1_1_imu.html#aaf94fb4f5ba28e8c471cfefaee673b3d", null ],
    [ "UnlockUserInterface", "classflair_1_1sensor_1_1_imu.html#a279bf62436c91f0ced11455bff38c269", null ],
    [ "UseDefaultPlot", "classflair_1_1sensor_1_1_imu.html#a81e08d2c2963d05ba8c38792af661527", null ],
    [ "GetPlotTab", "classflair_1_1sensor_1_1_imu.html#a0bfa24fb3b089499b33a60ae0162e4ee", null ],
    [ "GetOneAxisRotation", "classflair_1_1sensor_1_1_imu.html#ab399f291b7203463dbd1585db8e4b96c", null ],
    [ "GetGroupBox", "classflair_1_1sensor_1_1_imu.html#aa67100f97133a4d1eb4f3a225970eb85", null ],
    [ "ApplyRotation", "classflair_1_1sensor_1_1_imu.html#ad19d0939034366aafe24aceaf41a936f", null ],
    [ "ApplyRotation", "classflair_1_1sensor_1_1_imu.html#aaf735d7535414d32e023d5eef8d525fc", null ],
    [ "GetDatas", "classflair_1_1sensor_1_1_imu.html#aa020369da0c3f8c80eef6abf9cad0cd9", null ],
    [ "::Ahrs_impl", "classflair_1_1sensor_1_1_imu.html#a50c9130b6608a0ab2fd959a44c72dae6", null ]
];