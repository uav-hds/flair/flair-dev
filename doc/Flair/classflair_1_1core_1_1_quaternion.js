var classflair_1_1core_1_1_quaternion =
[
    [ "Quaternion", "classflair_1_1core_1_1_quaternion.html#a35151d7d2158d0a4714a66d2d426cf9c", null ],
    [ "~Quaternion", "classflair_1_1core_1_1_quaternion.html#a7aa88aa352e17590e0f9b454357fdbfc", null ],
    [ "GetNorm", "classflair_1_1core_1_1_quaternion.html#a6931ec913f61c9708ef5970e63f8b0b5", null ],
    [ "Normalize", "classflair_1_1core_1_1_quaternion.html#aa540ffe227bbacd124c77f95d8c720b3", null ],
    [ "GetLogarithm", "classflair_1_1core_1_1_quaternion.html#a4753cafd0c78d78a6e6d0200a4cc596f", null ],
    [ "GetLogarithm", "classflair_1_1core_1_1_quaternion.html#abbda897244c722294de3812bfe7a595b", null ],
    [ "Conjugate", "classflair_1_1core_1_1_quaternion.html#ae3cf1c1892b4d1c70ca055e42709f9df", null ],
    [ "GetConjugate", "classflair_1_1core_1_1_quaternion.html#ab7b4f5b1f7419f6179f4fccc49e4e173", null ],
    [ "GetDerivative", "classflair_1_1core_1_1_quaternion.html#a08b196fb32f87dd62e6a3906e5ecaf2c", null ],
    [ "Derivate", "classflair_1_1core_1_1_quaternion.html#a9be083f02e545535ba9dde0dd2ecbcb7", null ],
    [ "ToEuler", "classflair_1_1core_1_1_quaternion.html#a5d45a89dbbb73633701e344259e8b003", null ],
    [ "ToEuler", "classflair_1_1core_1_1_quaternion.html#ad98cdabf355956519d718aac330174ef", null ],
    [ "ToRotationMatrix", "classflair_1_1core_1_1_quaternion.html#a29d40f4afe4fa2f79a08a09619a4fc1a", null ],
    [ "operator+=", "classflair_1_1core_1_1_quaternion.html#a4f3a6012a315db41398417d0b93704ec", null ],
    [ "operator-=", "classflair_1_1core_1_1_quaternion.html#aec35455d090ebfdf8323b5019f9923bd", null ],
    [ "operator=", "classflair_1_1core_1_1_quaternion.html#a48f3e9b60017cbc8d7e9af035f85f3bf", null ],
    [ "q0", "classflair_1_1core_1_1_quaternion.html#a5394aa98e53d17253efd3c5eaca11fb2", null ],
    [ "q1", "classflair_1_1core_1_1_quaternion.html#aaaea36fb6b8a1ae444f879e509cb4db5", null ],
    [ "q2", "classflair_1_1core_1_1_quaternion.html#a47c497424771521c976143ef07bc7da1", null ],
    [ "q3", "classflair_1_1core_1_1_quaternion.html#aabf0e55b55e7c66716494c5d7f343e70", null ]
];