#!/bin/bash
NB_THREADS=$(nproc)
from_scratch=no
IDE_SCRIPT=cmake_codelite_outofsource.sh
toolchains=($OECORE_CMAKE_TOOLCHAINS)

function green_echo () {
	echo -e "\033[32m$1\033[0m"
}

function red_echo () {
	echo -e "\033[31m$1\033[0m"
}

function check_error () {
	if [ "$?" != "0" ]; then
		red_echo "Error, exiting"
		exit 1
	fi
}

function sanity_check () {
	if [ -z $FLAIR_ROOT ]; then
		red_echo "You must set the FLAIR_ROOT environement variable"
		exit 1
	fi

    if ! [ -d $FLAIR_ROOT/flair-build ]; then
		green_echo "Creating $FLAIR_ROOT/flair-build directory"
        mkdir -p $FLAIR_ROOT/flair-build
	fi
}


sanity_check

printf "Compile all from scratch (flair-build directory will be erased) [Y/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
	rm -rf $FLAIR_ROOT/flair-build/*
    cd $FLAIR_ROOT/flair-build
    $FLAIR_ROOT/flair-dev/scripts/$IDE_SCRIPT $FLAIR_ROOT/flair-src/
fi

#iterate over available toolchains
for arch in ${toolchains[@]}; do
    green_echo "Compiling and installing for $arch"
    cd $FLAIR_ROOT/flair-build/build_$arch
    make -j$NB_THREADS
    check_error	
    make install
done


printf "Compile Flair libs documentation [Y/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
	$OECORE_HOST_NATIVE_SYSROOT/usr/bin/doxygen $FLAIR_ROOT/flair-src/lib/Doxyfile.in
fi


exit 0
