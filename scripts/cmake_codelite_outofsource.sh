#!/bin/bash

function info () {
	echo -e "\033[32m$1\033[0m"
}

function warn () {
	echo -e "\033[33m$1\033[0m"
}

function err () {
	echo -e "\033[31m$1\033[0m"
	exit 1
}

if [ $(uname -m) != "x86_64" ]; then
    err "unsupported host architecture, you need a x86_64 cpu"
fi

if ! [ -f  ${1}/CMakeLists.txt ]; then
	err "${1} directory does not contain CMakeLists.txt"
fi

#get cmake
CMAKE=${OECORE_HOST_NATIVE_SYSROOT}/usr/bin/cmake

#verifie l'existence du lien symbolique build
if [ -d build ];then
	if ! readlink build > /dev/null ; then
		#it is a directory, exit to do not erase anything
		err "Error: build already exsits and is a directory; it should be a symlink."
	fi
	if ! readlink build | grep -w build_core2_64 >/dev/null; then
		warn "Warning, build was pointing to another directory."
	fi
	rm build
fi

#iterate over available toolchains
toolchains=($OECORE_CMAKE_TOOLCHAINS)
for arch in ${toolchains[@]}; do
    info "*************  Creating project for $arch *************"
    toolchain=$(eval "echo \"\$OECORE_CMAKE_${arch^^}_TOOLCHAIN\"")
    mkdir -p build_${arch}
    cd build_${arch}
    rm -f CMakeCache.txt
    rm -rf CMakeFiles
    ${CMAKE} ${1} -G "CodeLite - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${toolchain} -DCMAKE_CODELITE_USE_TARGETS=ON
    cd ..
done

#creation du lien symbolique
ln -s build_core2_64 build
